/* =======================================================================
 This file is created by E-merchant [ http://www.e-merchant.com/ ]
 In order to use this code (or a part of it), please feel free
 to contact E-merchant
 FILE NAME: app.js - Copyright (C)
======================================================================= */

// Include gulp
var gulp = require('gulp'),
    // Include Our Plugins
    jshint      = require('gulp-jshint'),
    sass        = require('gulp-sass'),
    concat      = require('gulp-concat'),
    uglify      = require('gulp-uglify'),
    watch       = require('gulp-watch'),
    rename      = require('gulp-rename'),
    newer       = require('gulp-newer'),
    imagemin    = require('gulp-imagemin'),
    cache       = require('gulp-cache'),
    livereload  = require('gulp-livereload'),
    //notify      = require('gulp-notify'),
    neat        = require('node-neat').includePaths,
    cssmin      = require('gulp-cssmin');

// Lint Task
gulp.task('lint', function() {
    return gulp.src('../js/*.js')
        .pipe(jshint())
        .pipe(jshint.reporter('default'));
        //.pipe(notify({ message: 'LINT task --> GOOD !' }));
});
// Html
gulp.task('html', function() {
    return gulp.src('../dist/*.html')
        //.pipe(gulp.dest('../dist/html'))
        .pipe(livereload());
        //.pipe(notify({ message: 'HTML files are reloaded!' }));
});
// Compile Sass
gulp.task('sass', function() {
    return gulp.src('../sass/*.scss')
        .pipe(sass({
            includePaths: ['stylesheet'].concat(neat),
            style: 'compressed'
        }))
        .pipe(gulp.dest('../dist/css'))
        .pipe(cssmin())
        .pipe(rename({suffix: '.min'}))
        .pipe(gulp.dest('../dist/css/'))
        .pipe(livereload());
        //.pipe(notify({ message: 'SASS task --> GOOD !' }));
});

// Images optimization
gulp.task('images', function() {
  return gulp.src('../dist/images/*.*')
        .pipe(newer('../dist/images/optimized/'))
        .pipe(cache(imagemin({ optimizationLevel: 5, progressive: true, interlaced: true })))
        .pipe(gulp.dest('../dist/images/optimized/'));
        //.pipe(notify({ message: 'IMAGES are now optimized. Task --> GOOD !' }) );
});

// Concatenate & Minify JS
gulp.task('scripts', function() {
    return gulp.src(['../js/libs/jquery/jquery-1.11.0.min.js',
                    '../js/app.js']) //Final file concatenated with the files written above
        .pipe(concat('../js/app.js'))
        .pipe(gulp.dest('../dist/js'))
        .pipe(rename('app.min.js'))
        .pipe(uglify())
        .pipe(livereload())
        .pipe(gulp.dest('../dist/js/'));
        //.pipe(notify({ message: 'SCRIPTS task --> GOOD !' }));
});

// Watch Files For Changes
gulp.task('watch', function() {
    gulp.watch('../js/*.js', ['lint', 'scripts']);
    gulp.watch('../sass/*.scss', ['sass']);
    gulp.watch('../dist/images/*.*', ['images']);
    gulp.watch('../dist/*.html', ['html']);
});

// Default Task
gulp.task('default', ['watch']); // gulp
