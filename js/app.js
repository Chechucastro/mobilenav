$(function(){
    var myObj = {
        click        : 'onTap' in window ? 'tap' : 'click',
        body         : $('body'),
        page         : $('.wrapper'),

        /* Navigation menu vars */
        menu         : $('.menu'),
        btnMenu      : $('.btnMenu'),
        clsLeft      : 'left',
        clsRight     : 'right',
        clsAnim      : 'animating',
        menuVisible  : 'menuIsVisible',
        transitionEnd: 'transitionend webkitTransitionEnd otransitionend MSTransitionEnd', /* Cross browser support for CSS "transition end" event */


        init: function() {
            this.moduleNavigationMenu.init();
        },
        moduleNavigationMenu : {
            init : function(){
                this.toggleNavigation();
            },
            toggleNavigation : function(){
                /* Start animation when menu button is clicked */
                myObj.btnMenu.on(myObj.click, function(e) {
                    e.preventDefault();

                    /* Animating the body... */
                    myObj.body.addClass(myObj.clsAnim);

                    /* Switching left or right css classes to the body tag */
                    w = (myObj.body.hasClass(myObj.menuVisible)) ? myObj.body.addClass(myObj.clsLeft) : myObj.body.addClass(myObj.clsRight);

                    /* Remove any animating classes */
                    myObj.page.on(myObj.transitionEnd, function() {
                        myObj.body.removeClass(''+myObj.clsAnim+' '+myObj.clsLeft+' '+myObj.clsRight+'').toggleClass(myObj.menuVisible);
                        myObj.page.off(myObj.transitionEnd);
                    });
                });
            }
        }
    };
        window.myObj = myObj;
        myObj.init();
});
