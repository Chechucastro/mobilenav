Mobile Navigation
===================

>  This is an easy navigation for mobiles made by Chechu Castro.
>  Feel free to contact me at chechu@digitatis.com
https://bitbucket.org/Chechucastro/
https://github.com/chechucastro

Table of contents:

----------
HTML:
```
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Mobile nav</title>
    <meta name="viewport" content="width=device-width,minimum-scale=1,maximum-scale=1">
    <link rel="stylesheet" href="./css/common.css">
</head>
<body>
    <nav>
        <ul class="menu">
            <li><a title="Home" href="#">Home</a></li>
            <li><a title="About Me" href="#">About Me</a></li>
            <li><a title="Contact Me" href="#">Contact Me</a></li>
        </ul>
    </nav>
    <div class="wrapper">
        <header>
            <a href="#" class="btnMenu">Menu</a>
        </header>
        <div class="main">
            Lorem ipsum dolor sit amet, consectetur adipisicing elit.
            Alias voluptate ad velit perspiciatis ipsa! Nostrum voluptate ex at laborum esse culpa modi,
            molestiae eveniet numquam tenetur expedita quis aspernatur doloribus!
        </div>
    </div>
    <script src="./js/app.js" type="text/javascript" charset="utf-8"></script>
</body>
</html>
```
SASS:
```
/* Base */
html, body {
    margin: 0;
    padding: 0;
    height: 100%;
    background: #000;
    /* Same color as your menu */
    font-family: Helvetica, Arial, sans-serif;
}
nav{
    .menu {
        display: none;
        position: absolute;
        top: 0;
        left: 0;
        width: 190px;
        padding: 15px 25px;
        margin: 0;
        list-style: none;
        background: #000;
        z-index: 10;
       a {
        display: block;
        color: #fff;
        padding: 15px 0;
        border-bottom: 1px solid rgba( 255, 255, 255, 0.05 );
        }
    }
}

.wrapper {
    position: relative;
    z-index: 20;
    padding: 20px;
    background: #fff;
    height: 100%;
    header{
        .btnMenu {
            position: absolute;
            top: 10px;
            left: 10px;
            padding: 15px 10px;
            border: 1px solid #eee;
        }
    }
    /* Main body */
    .main{
        padding-top: 18%;
    }
}

/* Animations */
.wrapper, .menu {
    -webkit-backface-visibility: hidden;
    -webkit-perspective: 1000;
}

/* Hide the menu unless it's animating or visible */
.animating .menu, .menuIsVisible .menu {
    display: block;
}

.animating .wrapper {
    transition: transform .25s ease-in-out;
    -webkit-transition: -webkit-transform .25s ease-in-out;
}

.animating.left .wrapper {
    transform: translate3d( -80%, 0, 0 );
    -webkit-transform: translate3d( -80%, 0, 0 );
}
.animating.right .wrapper {
    transform: translate3d( 80%, 0, 0 );
    -webkit-transform: translate3d( 80%, 0, 0 );
}
.menuIsVisible .wrapper {
    left: 80%;
}

```
JavaScript/JQuery
```
$(function(){
    var myObj = {
        click        : 'onTap' in window ? 'tap' : 'click',
        body         : $('body'),
        page         : $('.wrapper'),

        /* Navigation menu vars */
        menu         : $('.menu'),
        btnMenu      : $('.btnMenu'),
        clsLeft      : 'left',
        clsRight     : 'right',
        clsAnim      : 'animating',
        menuVisible  : 'menuIsVisible',
        transitionEnd: 'transitionend webkitTransitionEnd otransitionend MSTransitionEnd', /* Cross browser support for CSS "transition end" event */

        init: function() {
            this.moduleNavigationMenu.init();
        },
        moduleNavigationMenu : {
            init : function(){
                this.toggleNavigation();
            },
            toggleNavigation : function(){
                /* Start animation when menu button is clicked */
                myObj.btnMenu.on(myObj.click, function(e) {
                    e.preventDefault();

                    /* Animating the body... */
                    myObj.body.addClass(myObj.clsAnim);

                    /* Switching left or right css classes to the body tag */
                    w = (myObj.body.hasClass(myObj.menuVisible)) ? myObj.body.addClass(myObj.clsLeft) : myObj.body.addClass(myObj.clsRight);

                    /* Remove any animating classes */
                    myObj.page.on(myObj.transitionEnd, function() {
                        myObj.body.removeClass(''+myObj.clsAnim+' '+myObj.clsLeft+' '+myObj.clsRight+'').toggleClass(myObj.menuVisible);
                        myObj.page.off(myObj.transitionEnd);
                    });
                });
            }
        }
    };
        window.myObj = myObj;
        myObj.init();
});

```
